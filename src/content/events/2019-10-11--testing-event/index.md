---
path: /events/first-event
date: "2019-10-15"
author: "Test Author"
title: "Welcome to the first event of FOSSNSS"
cover: "./mount.jpg"
---

# Welcome

Hey this is our test event

![mountain](./mount.jpg)

This is the Test event item.

- Hi this is an unordered list
- I don't know what else to say
- Maybe, make it look like something important for the event
